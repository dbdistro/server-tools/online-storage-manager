import os

from flask import render_template, flash, redirect, url_for, request, send_from_directory, send_file
from app import app, db
from app.forms import LoginForm, SignUpForm, SettingsAccount, SettingsMoreSpace, SettingsPassword
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User
from app.dirMan import FileManagement
from werkzeug.utils import secure_filename


import sqlalchemy as sa
import zipfile
 

""" FILE MANAGER """
@app.route('/root', defaults={'req_path': ''}, methods=['GET', 'POST'])
@app.route('/root/', defaults={'req_path': ''}, methods=['GET', 'POST'])
@app.route('/root/<path:req_path>', methods=['GET', 'POST'])
@login_required
def index(req_path):
    # What dir is the user in.
    files = FileManagement(current_user.id, req_path)

    # Downloads files to user's computer.
    if request.method == 'POST':
        zip_file = files.download(request.form).replace('app/', '')
        # return send_file(zip_file, as_attachment=True)
        return send_file(zip_file)
    # debug
    # print(request.path)
    
    return render_template('file_manager.html', title="Files/Dirs", files=files.list_dirs(), os=os)


@app.route('/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    # Checks if form filled out.
    if form.validate_on_submit():
        user = db.session.scalar(sa.select(User).where(sa.func.lower(User.username) == str(form.username.data).lower()))
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title="Login", form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect((url_for('login')))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = SignUpForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)

        db.session.add(user)
        db.session.commit()
        flash('Account Created, Login using your credentials.')

        return redirect(url_for('login'))

    return render_template('signup.html', title='Sign Up', form=form)

"""---------------------------- SETTINGS ----------------------------"""
# @app.route('/settings', methods=['GET', 'POST'])
# @login_required
# def settings():
#     settingsAccount = SettingsAccount(current_user.username, current_user.email)
#     settingsPassword = SettingsPassword()
#     settingsMoreSpace = SettingsMoreSpace()
#     
# 
#     # Debug
#     # print('-'*100)
#     # print(settingsAccount.username.errors)
#     # print(settingsAccount.email.errors)
#     # print(settingsAccount.validate_on_submit())
#     # print('-'*100)
# 
#     if settingsAccount.validate_on_submit():
#         current_user.username = settingsAccount.username.data
#         current_user.email = settingsAccount.email.data
#         db.session.commit()
#         flash('Account Changes Saved')
# 
#     elif settingsPassword.validate_on_submit():
#         if current_user.check_password(settingsPassword.old_password.data):
#             # Check if new password fields match.
#             if settingsPassword.new_password.data == settingsPassword.new_password_2.data:
#                 current_user.set_password(settingsPassword.new_password.data)
#                 db.session.commit()
#                 flash('Password Changed')
#         else:
#             flash('Invalid Old Password')
# 
#     elif settingsMoreSpace.validate_on_submit():
#         flash('Request Sent to Server Administrator')
#         
# 
# 
#     # Get username
#     settingsAccount.username.data = current_user.username
#     # Get email
#     settingsAccount.email.data = current_user.email
# 
#     return render_template('settings.html', title='Settings', settingsAccount=settingsAccount, settingsPassword=settingsPassword, settingsMoreSpace=settingsMoreSpace)
    
@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    settingsAccount = SettingsAccount(current_user.username, current_user.email)
    settingsPassword = SettingsPassword()
    settingsMoreSpace = SettingsMoreSpace()        


    # Get username
    settingsAccount.username.data = current_user.username
    # Get email
    settingsAccount.email.data = current_user.email

    return render_template('settings.html', title='Settings', settingsAccount=settingsAccount, settingsPassword=settingsPassword, settingsMoreSpace=settingsMoreSpace)


@app.route('/settings/account', methods=['GET', 'POST'])
@login_required
def settingsAccount():
    settingsAccount = SettingsAccount(current_user.username, current_user.email)
    settingsPassword = SettingsPassword()
    settingsMoreSpace = SettingsMoreSpace()
    

    # Debug
    # print('-'*100)
    # print(settingsAccount.username.errors)
    # print(settingsAccount.email.errors)
    # print(settingsAccount.validate_on_submit())
    # print('-'*100)

    if settingsAccount.validate_on_submit():
        current_user.username = settingsAccount.username.data
        current_user.email = settingsAccount.email.data
        db.session.commit()
        flash('Account Changes Saved')

    # Get username
    settingsAccount.username.data = current_user.username
    # Get email
    settingsAccount.email.data = current_user.email

    return render_template('settings.html', title='Settings', settingsAccount=settingsAccount, settingsPassword=settingsPassword, settingsMoreSpace=settingsMoreSpace)


@app.route('/settings/password', methods=['GET', 'POST'])
@login_required
def settingsPassword():
    settingsAccount = SettingsAccount(current_user.username, current_user.email)
    settingsPassword = SettingsPassword()
    settingsMoreSpace = SettingsMoreSpace()

    
    if settingsPassword.validate_on_submit():
        if current_user.check_password(settingsPassword.old_password.data):
            # Check if new password fields match.
            if settingsPassword.new_password.data == settingsPassword.new_password_2.data:
                current_user.set_password(settingsPassword.new_password.data)
                db.session.commit()
                flash('Password Changed')
        else:
            flash('Invalid Old Password')


    # Get username
    settingsAccount.username.data = current_user.username
    # Get email
    settingsAccount.email.data = current_user.email

    return render_template('settings.html', title='Settings', settingsAccount=settingsAccount, settingsPassword=settingsPassword, settingsMoreSpace=settingsMoreSpace)


@app.route('/settings/requestMoreSpace', methods=['GET', 'POST'])
@login_required
def settingsRequestMoreSpace():
    settingsAccount = SettingsAccount(current_user.username, current_user.email)
    settingsPassword = SettingsPassword()
    settingsMoreSpace = SettingsMoreSpace()
 

    if settingsMoreSpace.validate_on_submit():
        flash('Request Sent to Server Administrator')

    # Get username
    settingsAccount.username.data = current_user.username
    # Get email
    settingsAccount.email.data = current_user.email

    return render_template('settings.html', title='Settings', settingsAccount=settingsAccount, settingsPassword=settingsPassword, settingsMoreSpace=settingsMoreSpace)

