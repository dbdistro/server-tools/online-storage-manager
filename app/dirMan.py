import os, time, humanize, shutil, zipfile
from datetime import datetime



class FileManagement:
    def __init__(self, user_id, path):
        self.user_id = str(user_id)
        self.user_path = os.path.join("storage", self.user_id)
        self.root_path = os.path.join(self.user_path, "root")
        self.tmp_path = os.path.join('app/tmp', self.user_id)

        self.cwd = os.path.join(f'{self.root_path}', path) # current working directory 


        print(f'sel.cwd: {self.cwd}')
        print(f'sel.path: {path}')
        # Check to see if directory exists (Check to see if user has a dir)
        # if not create one. Create a dir to store users files.
        if os.path.isdir(self.root_path) == False:
            # Create dir
            os.makedirs(self.root_path)

    def list_dirs(self):
        """ Collect information on files and dirs then return them in a dictionary format. """
        # dirs = [{'name': 'foo.py', 'dir': False, 'size': '80 bytes', 'date_modified': '2024/03/17'}]
        files = os.listdir(self.cwd)

        files_formatted = []

        for file in files:
            # is it a directory=True or file=False.
            dir = os.path.isdir(os.path.join(self.cwd, file))

            # SIZE
            size = humanize.naturalsize(os.path.getsize(os.path.join(self.cwd, file)), binary=True).replace('i', '')

            
            # DATE MODIFIED
            # Displays in UTC time. EXAMPLE: 2024-03-23 23:12:25
            date_modified = datetime.strptime(time.ctime(os.path.getctime(os.path.join(self.cwd, file))), "%a %b %d %H:%M:%S %Y")

            files_formatted.append({'name': f"{file}", 'dir': dir, 'size': size, 'date_modified': f"{date_modified} UTC" })

        return files_formatted


    def download(self, form):
        """ Copies the files to a temp directory for download. """
        
        # Get downloads
        downloads = []
        for file in self.list_dirs():
            if form.get(file['name']) == 'on':
                filepath = f"{os.path.join(self.cwd, file['name'])}"
                # Remove app/
                downloads.append(filepath)
        print(downloads)

        # Create zip then prompt for download.
        if len(downloads) > 0:
            # Create a tmp(temporary) directory if it does not exist.
            if os.path.isdir(self.tmp_path) == False:
                os.makedirs(self.tmp_path, exist_ok=True)
            else:
                self.deleteTMP() # Deletes tmp files.
                os.makedirs(self.tmp_path, exist_ok=True)
            print(self.tmp_path)
            
            # Copy files to tmp dir.
            for file in downloads:
                # Copy files to tmp dir.
                if os.path.isdir(file):
                    shutil.copytree(file, self.tmp_path)
                else:
                    shutil.copy(file, self.tmp_path)

            
            # Zip the files
            os.chdir(self.tmp_path)
            print("this is the curret", os.getcwd())
            zip_file = os.path.join('extractMe.zip')
            print(zip_file)
            with zipfile.ZipFile(zip_file, 'w') as zipf:
                for file in os.listdir():
                    # add to zip
                    print(file)
                    zipf.write(file)
            os.chdir(os.path.dirname(os.path.abspath(__file__)))
                
            # Download files
            return zip_file

            # return send_file(zip_file, as_attachment=True)



            # DEBUG
            # print(file['name'])
            # print(form)

        # form = request.form.get('form_files-to-download')


    def deleteTMP(self):
        """ Deletes tmp files. """
        shutil.rmtree(self.tmp_path)
