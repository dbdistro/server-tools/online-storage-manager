from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, TextAreaField
from wtforms.validators import DataRequired, EqualTo, Email, ValidationError
import sqlalchemy as sa
from app import db
from app.models import User


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Login')

class SignUpForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Create Account')


    def validate_username(self, username):
        user = db.session.scalar(sa.select(User).where(sa.func.lower(User.username) == str(username.data).lower()))
        if user is not None:
            raise ValidationError('Username Taken, try another.')


    def validate_email(self, email):
        user = db.session.scalar(sa.select(User).where(sa.func.lower(User.email) == str(email.data).lower()))
        if user is not None:
            raise ValidationError('Email already in use.')


class SettingsAccount(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Save')


    def __init__(self, original_username, original_email, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.original_username = original_username
        self.original_email = original_email

    def validate_username(self, username):
        if username.data.lower() != self.original_username.lower():
            user = db.session.scalar(sa.select(User).where(sa.func.lower(User.username) == str(username.data).lower()))
            if user is not None:
                raise ValidationError('Username Taken, try another.')


    def validate_email(self, email):
        if email.data.lower() != self.original_email.lower():
            user = db.session.scalar(sa.select(User).where(sa.func.lower(User.email) == str(email.data).lower()))
            if user is not None:
                raise ValidationError('Email already in use.')


class SettingsPassword(FlaskForm):
    old_password = PasswordField('Old Password', validators=[DataRequired()])
    new_password = PasswordField('New Password', validators=[DataRequired()])
    new_password_2 = PasswordField('Repeat New Password', validators=[DataRequired(), EqualTo('new_password')])
    submit_password = SubmitField('Change Password')


class SettingsMoreSpace(FlaskForm):
    amount = IntegerField('Additional Space Amount', validators=[DataRequired()])
    reason = TextAreaField('Reason for Additional Space in GB', validators=[DataRequired()])
    submit_moreSpace = SubmitField('Send')
